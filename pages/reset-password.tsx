import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import ResetPasswordContainer from '../containers/ResetPasswordContainer'
import { HomeProvider } from '../providers/homeProvider'

const Signup: NextPage = () => {
    const headList = {
        title: 'Signup | Koperasi Nusantara',
        description: 'This reset password page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <ResetPasswordContainer/>
            </AppLayout>
        </HomeProvider>
    )
}

export default Signup
