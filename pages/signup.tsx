import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import SignupContainer from '../containers/SignupContainer'
import { HomeProvider } from '../providers/homeProvider'

const Signup: NextPage = () => {
    const headList = {
        title: 'Signup | Koperasi Nusantara',
        description: 'This signup page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <SignupContainer/>
            </AppLayout>
        </HomeProvider>
    )
}

export default Signup
