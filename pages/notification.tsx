import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import NotificationContainer from '../containers/NotificationContainer'
import { HomeProvider } from '../providers/homeProvider'

const Home: NextPage = () => {
    const headList = {
        title: 'Home | Koperasi Nusantara',
        description: 'This notification page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <NotificationContainer />
            </AppLayout>
        </HomeProvider>
    )
}

export default Home
