import db from "../../../utils/db";

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req, res) => {
    if (req.method == "POST") {
        try {
            const { user_id } = req.body;
            const entries = await db.collection("users");
            const queryRes = await entries
                .where("user_id", "==", user_id)
                .limit(1)
                .get();

            if (queryRes.docs.length > 0) {
                const response = await db
                    .collection("users")
                    .doc(queryRes.docs[0].id)
                    .update({
                        ...req.body,
                        updated: new Date().toISOString(),
                    });
                res.status(200).json(response);
            } else {
                const { id } = await db.collection("users").add({
                    ...req.body,
                    created: new Date().toISOString(),
                });
                res.status(200).json({ id });
            }
        } catch (e) {
            console.log(e, "error?????");
            res.status(400).end();
        }
    } else {
        res.status(405).json({ msg: "Method not allowed" });
    }
};
