import db from "../../../utils/db";

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req, res) => {
    if (req.method == "GET") {
        try {
            const { user_id } = req.query;
            const entries = await db.collection("fundings");
            const queryRes = await entries
                .where("user_id", "==", user_id)
                .get();
            res.status(200).json(queryRes.docs.map((doc) => doc.data()));
        } catch (e) {
            console.log(e, "error?????");
            res.status(400).end();
        }
    } else {
        res.status(405).json({ msg: "Method not allowed" });
    }
};
