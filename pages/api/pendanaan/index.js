import db from "../../../utils/db";

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req, res) => {
    if (req.method == "POST") {
        try {
            const { id } = await db.collection("fundings").add({
                ...req.body,
                created: new Date().toISOString(),
            });
            res.status(200).json({ id });
        } catch (e) {
            console.log(e, "error?????");
            res.status(400).end();
        }
    } else {
        res.status(405).json({ msg: "Method not allowed" });
    }
};
