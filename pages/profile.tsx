import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import ProfileContainer from '../containers/ProfileContainer'
import { HomeProvider } from '../providers/homeProvider'

const Profile: NextPage = () => {
    const headList = {
        title: 'Signup | Koperasi Nusantara',
        description: 'This profile page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList} background="bg-gradient-to-r from-[#4950F9] to-[#1937FE]">
                <ProfileContainer/>
            </AppLayout>
        </HomeProvider>
    )
}

export default Profile
