import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import HomeContainer from '../containers/HomeContainer'
import { HomeProvider } from '../providers/homeProvider'

const Home: NextPage = () => {
    const headList = {
        title: 'Home | Koperasi Nusantara',
        description: 'This home page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <HomeContainer />
            </AppLayout>
        </HomeProvider>
    )
}

export default Home
