import { useState } from 'react'
import { Dialog } from '@headlessui/react'

interface ModalProps {
    title: string
    content: string
    isOpen: boolean
    setIsOpen: (state:boolean) => void
}

const Modal: React.FC<ModalProps> = ({ title, content, isOpen, setIsOpen }) => {

  return (
    <Dialog open={isOpen} onClose={() => setIsOpen(false)} className="absolute z-50 flex items-center justify-center w-full h-full top-10">
      <Dialog.Panel className={"bg-blue-400 w-60 text-white p-5 rounded-lg relative flex flex-col items-center gap-2"}>
        <Dialog.Title className={"font-bold"}>{title}</Dialog.Title>
        <Dialog.Description>
          {content}
        </Dialog.Description>
        <button onClick={() => setIsOpen(false)} className="px-2 py-1 bg-blue-200 rounded w-fit">Close</button>
      </Dialog.Panel>
    </Dialog>
  )
}

export default Modal