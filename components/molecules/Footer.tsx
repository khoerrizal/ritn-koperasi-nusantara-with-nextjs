import Link from "next/link"
import NotificationIcon from "../atoms/NotificationIcon"
import ProfileIcon from "../atoms/ProfileIcon"
import WalletIcon from "../atoms/WalletIcon"

interface FooterProps {
    activeTab: "wallet" | "notification" | "profile"
}

const Footer: React.FC<FooterProps> = ({ activeTab }) => {
    return (
        <div className="absolute bottom-0 z-30 flex flex-row items-center w-full h-24 bg-white shadow-2xl justify-evenly">
            <Link href={"/"}>
                <div className="flex flex-col items-center justify-center gap-2 cursor-pointer">
                    <WalletIcon active={activeTab == "wallet"}/>
                    <div className={`${activeTab == "wallet" ? "bg-[#2B47FC]" : "bg-transparent" } h-2 w-2 rounded`}/>
                </div>
            </Link>
            <Link href={"/notification"}>
                <div className="flex flex-col items-center justify-center gap-2">
                    <NotificationIcon active={activeTab == "notification"}/>
                    <div className={`${activeTab == "notification" ? "bg-[#2B47FC]" : "bg-transparent" } h-2 w-2 rounded`}/>
                </div>
            </Link>
            <Link href={"/profile"}>
                <div className="flex flex-col items-center justify-center gap-2 cursor-pointer">
                    <ProfileIcon active={activeTab == "profile"}/>
                    <div className={`${activeTab == "profile" ? "bg-[#2B47FC]" : "bg-transparent" } h-2 w-2 rounded`}/>
                </div>
            </Link>
        </div>
    )
}

export default Footer