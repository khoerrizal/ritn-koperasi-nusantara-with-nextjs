import Head from 'next/head'
import Footer from '../../molecules/Footer'

export interface AppLayoutProps {
    title: string
    description: string
    children: React.ReactNode
    background?: string
}

const AppLayout: React.FC<AppLayoutProps> = ({ title, description, children, background="" }) => {
    return (
        <div className="flex flex-col">
            <Head>
                <title>{title}</title>
                <meta name="description" content={description} />
            </Head>

            <main className={`${background} min-h-[812px] max-w-[375px] w-full flex justify-center mx-auto overflow-hidden relative`}>
                {children}
            </main>

        </div>
    )
}

export default AppLayout
