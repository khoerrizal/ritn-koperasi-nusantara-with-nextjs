import { useRouter } from "next/router";
import React, { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useGlobalContext } from "../../providers/globalProvider";
import { backgroundLogin, buttonMask } from "../../public/images/LoginPage";
import Image from "next/image";
import Link from "next/link";

interface SignupType {
    email: string;
    password: string;
    password_confirm: string;
}
const SignupContainer = () => {
    const router = useRouter();
    const {
        authState: { signUp }
    } = useGlobalContext()
    const [inputEmail, setInputEmail] = useState<string>("")
    const [inputPasword, setInputPassword] = useState<string>("")
    const [errorMsg, setErrorMsg] = useState<string>("")

    const submitSignup = async (event:any) => {
        event.preventDefault()
        try {
            const res = await signUp(inputEmail, inputPasword);
            router.push("/profile");
        } catch (error: any) {
            console.log(error.message, "error on signup");
            if (error.message.indexOf("email-already-in-use") != -1){
                setErrorMsg("Email already in use")
            }
        }
    };

    const handleChangeEmail = (event: any) => {
        setInputEmail(event.target.value)
    }

    const handleChangePassword = (event: any) => {
        setInputPassword(event.target.value)
    }

    return (
        <div className="px-8 py-16 w-[375px] relative h-[812px] items-end">
            <Image src={backgroundLogin} alt={"Background Login"} className="absolute left-0 -top-10"/>
            <form onSubmit={submitSignup} className="flex flex-col justify-end h-full gap-6">
                <span className="text-2xl font-bold">Daftar</span>
                <div className="flex flex-col">
                    <label className={`${errorMsg ? "text-red-500" : "text-[#B9B9B9]"} font-normal text-sm`}>Email</label>
                    <input onChange={handleChangeEmail} className={`${errorMsg ? "border-red-500" : "border-[#2743FD]"} border-b p-1`} name="field_email"/>
                    {errorMsg && (
                        <span className="text-red-500">{errorMsg}</span>
                    )}
                </div>
                <div className="flex flex-col">
                    <label className="text-[#B9B9B9] font-normal text-sm">Password</label>
                    <input onChange={handleChangePassword} className="border-[#2743FD] border-b p-1" name="field_password" type="password"/>
                </div>
                <Link href={"/reset-password"}><span className="text-[#2B47FC] cursor-pointer">Lupa Password?</span></Link>
                <button className="relative font-normal text-xl bg-gradient-to-r from-[#4960F9] to-[#1433FF] text-white p-6 text-left rounded-[28px]" type="submit">
                    <span>Daftar</span>
                    <Image src={buttonMask} alt={"Mask"} className="absolute top-0 right-0 w-full h-20"/>
                </button>
                <Link href={"/login"}><span className="text-[#2B47FC] cursor-pointer">Sudah punya akun?</span></Link>
            </form>
        </div>
    );
};

export default SignupContainer;