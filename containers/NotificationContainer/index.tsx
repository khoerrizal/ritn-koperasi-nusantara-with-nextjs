import { backgroundProfile } from "../../public/images/ProfilePage"
import Image from "next/image"
import Footer from "../../components/molecules/Footer"

const NotificationContainer: React.FC = () => {
    return (
        <div className="flex flex-col items-center w-full">
            <Image src={backgroundProfile} alt={"Background profile"} className="absolute top-0 right-0 z-0 object-cover"/>
            {/* <div onClick={handleBack} className="relative w-full cursor-pointer">
                <Image src={leftArrow} alt={"Back button"} className="absolute z-0 object-cover top-16 left-5"/>
            </div> */}
            <span className="z-10 w-full ml-5 text-4xl font-bold text-left mt-11">Notifikasi</span>
            <div className="z-10 flex items-center justify-center w-full h-full">
                <span>Tidak ada notifikasi</span>
            </div>
            <Footer activeTab={"notification"}/>
        </div>
    )
}

export default NotificationContainer