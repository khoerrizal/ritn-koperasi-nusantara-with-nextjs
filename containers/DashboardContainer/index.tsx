import { useRouter } from "next/router";
import { useEffect } from "react";
import { useCookies } from "react-cookie";

const DashboardContainer = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['user']);
    const router = useRouter();

    // protect page
    useEffect(() => {
        if (!cookies.user){
            router.push('/login')
        }
    }, []);

    return (
        <div className="container flex py-2 mx-auto">
            <div className="px-12 py-24 mx-auto mt-24 overflow-y-hidden text-gray-600">
                <h2 className="text-2xl font-semibold">You are logged in!</h2>
            </div>
        </div>
    );
  };
  
  export default DashboardContainer;