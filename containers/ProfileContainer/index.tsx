import Image from "next/image"
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { leftArrow } from "../../public/images/Arrow"
import { backgroundProfile, checkIcon, photoProfile } from "../../public/images/ProfilePage"
import CheckIconComponent from "./components/CheckIconComponent";
import axios from 'axios';
import { useGlobalContext } from "../../providers/globalProvider";
import Footer from "../../components/molecules/Footer";
import { storage } from '../../pages/config/firebase';
import { ref } from "@firebase/storage";
import { getDownloadURL, uploadBytesResumable } from "firebase/storage";
import Modal from "../../components/atoms/Modal";

const ProfileContainer = () => {
    const router = useRouter()
    const handleBack = () => {
        router.push("/")
    }
    
    const {
        authState: { user },
        contentState: {
            profileState,
            getProfile
        }
    } = useGlobalContext()

    const inputPhotoRef = useRef<
        HTMLInputElement | undefined
    >() as React.MutableRefObject<HTMLInputElement>

    const handleClickUploadPhoto = () => {
        inputPhotoRef.current.click()
    }

    const [photoValue, setPhotoValue] = useState<any>()
    const [photoBinary, setPhotoBinary] = useState<any>()
    const [disableSubmit, setDisableSubmit] = useState<boolean>(true)
    const [inputName, setInputName] = useState<string>("")
    const [inputDate, setInputDate] = useState<string>("")
    const [progresspercent, setProgresspercent] = useState<number>(0);
    const [imgUrl, setImgUrl] = useState<string | null>(null);
    const [isOpen, setIsOpen] = useState(false)
    const [resTitle, setResTitle] = useState<string>("")
    const [resContent, setResContent] = useState<string>("")

    const handleOnchangePhotoImg = (event: any) => {
        setPhotoValue(URL.createObjectURL(event.target.files[0]))
        setPhotoBinary(event.target.files[0])
        const file = event.target.files[0]
        const storageRef = ref(storage, `files/${file ? file.name : ""}`);
        const uploadTask = uploadBytesResumable(storageRef, file);

        uploadTask.on("state_changed",
            (snapshot) => {
                const progress =
                Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                setProgresspercent(progress);
            },
            (error) => {
                alert(error);
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                setImgUrl(downloadURL)
                });
            }
        )
    }

    const handleOnchangeName = (event: any) => {
        setInputName(event.target.value)
    }

    const handleOnchangeDate = (event: any) => {
        setInputDate(event.target.value)
    }

    const submitProfile = async (event:any) => {
        event.preventDefault()
        
        const data = { 
            name: inputName ? inputName : profileState?.name, 
            date_of_birth: inputDate ? inputDate : profileState?.date_of_birth,
            user_id: user.uid,
            image_url: imgUrl ? imgUrl : profileState?.image_url,
            email: user.email
        }
        const res = await axios.post('/api/users', data);
        setIsOpen(true)
        if (res.status == 200){
            getProfile(user.uid)
            setResTitle("Success!")
            setResContent("Your profile has been updated.")
        }else{
            setResTitle("Error!")
            setResContent("Something went wrong, please try again later.")
        }
    }

    useEffect(() => {
        if((inputName!="" || profileState?.name) && 
            (inputDate!='' || profileState?.date_of_birth) && 
            (photoValue || profileState?.image_url)
            ){
            setDisableSubmit(false)
        }else{
            setDisableSubmit(true)
        }
    }, [inputName, inputDate, photoValue, profileState?.name, profileState?.date_of_birth, profileState?.image_url]);
    
    return (
        <div className="flex flex-col items-center w-full">
            <Modal title={resTitle} content={resContent} isOpen={isOpen} setIsOpen={setIsOpen}/>
            <Image src={backgroundProfile} alt={"Background profile"} className="absolute top-0 right-0 z-0 object-cover"/>
            {/* <div onClick={handleBack} className="relative w-full cursor-pointer">
                <Image src={leftArrow} alt={"Back button"} className="absolute z-0 object-cover top-16 left-5"/>
            </div> */}
            <span className="z-10 w-full ml-5 text-4xl font-bold text-left mt-11">Profile</span>
            <div className="relative cursor-pointer" onClick={handleClickUploadPhoto}>
                <Image src={photoValue ?? profileState?.image_url ?? photoProfile} alt={"Photo profile"} className="z-30 object-cover mt-8 w-36 h-36 rounded-[36px]" width={144} height={144}/>
            </div>
            <input type="file" className="hidden" onChange={handleOnchangePhotoImg} ref={inputPhotoRef}/>
            <form onSubmit={submitProfile} className="z-30 w-full px-8 mt-10 text-[#80E0FF] font-normal text-sm justify-between h-full flex flex-col pb-28">
                <div className="flex flex-col gap-6">
                    <div className="flex flex-col gap-4">
                        <label>Nama Lengkap</label>
                        <input defaultValue={profileState?.name} onChange={handleOnchangeName} className="p-1 bg-transparent border-b border-b-white"/>
                    </div>
                    <div className="flex flex-col gap-4">
                        <label>Tanggal Lahir</label>
                        <input defaultValue={profileState?.date_of_birth} onChange={handleOnchangeDate} type="date" className="p-1 bg-transparent border-b border-b-white"/>
                    </div>
                </div>
                <button type="submit" className={`flex flex-row px-24 py-6 bg-white rounded-3xl ${disableSubmit ? "text-[#C8C8C8]" : "text-[#2B47FC]"} gap-3 justify-center items-center`} disabled={disableSubmit}>
                    <span>Simpan</span>
                    <div className="relative">
                        <CheckIconComponent active={!disableSubmit}/>
                    </div>
                </button>
            </form>
            {
                !imgUrl &&
                <div className='outerbar'>
                    <div className='innerbar' style={{ width: `${progresspercent}%` }}>{progresspercent}%</div>
                </div>
            }
            <Footer activeTab={"profile"}/>
        </div>
    )
}

export default ProfileContainer