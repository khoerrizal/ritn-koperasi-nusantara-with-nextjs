interface CheckProps {
    active: boolean
}

const CheckIconComponent: React.FC<CheckProps> = ({ active }) => {
    return (
        <svg width="18" height="13" viewBox="0 0 18 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.7363 0.259254C17.3848 -0.086418 16.815 -0.086418 16.4635 0.259254L5.68095 10.8634L1.53629 6.78737C1.18484 6.4417 0.615024 6.44173 0.263497 6.78737C-0.0879951 7.13301 -0.0879951 7.69339 0.263497 8.03906L5.04453 12.7409C5.39588 13.0865 5.96612 13.0863 6.31733 12.7409L17.7363 1.51097C18.0878 1.16534 18.0877 0.604926 17.7363 0.259254Z" fill={active ? "#2B47FC" : "#CFCFCF"}/>
        </svg>
    )
}

export default CheckIconComponent