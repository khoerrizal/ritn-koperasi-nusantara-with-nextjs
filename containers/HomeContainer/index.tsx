import Footer from "../../components/molecules/Footer";
import { useHomeContext } from "../../providers/homeProvider";
import FormPendanaan from "./components/FormPendanaan";
import HeaderSection from "./components/HeaderSection";
import PembayaranScreen from "./components/PembayaranScreen";
import SimpanPinjamScreen from "./components/SimpanPinjamScreen";
import TransaksiScreen from "./components/TransaksiScreen";

const HomeContainer: React.FC = () => {
    const {
        contentState: {
            activeTab
        }
    } = useHomeContext()
    return (
        <div className="relative w-full">
            <HeaderSection/>
            {activeTab == "simpan-pinjam" && (
                <SimpanPinjamScreen/>
            )}
            {activeTab == "transaksi" && (
                <TransaksiScreen/>
            )}
            {activeTab == "pembayaran" && (
                <PembayaranScreen/>
            )}
            {activeTab == "pendanaan" && (
                <FormPendanaan/>
            )}
            <Footer activeTab={"wallet"}/>
        </div>
    )
}

export default HomeContainer