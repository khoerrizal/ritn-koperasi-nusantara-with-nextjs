import Image from "next/image"
import { useMemo } from "react"
import { useGlobalContext } from "../../../providers/globalProvider"
import { useHomeContext } from "../../../providers/homeProvider"
import { HamburgerMenu } from "../../../public/images/HomePage"
import { photoProfile } from "../../../public/images/ProfilePage"
import { TabTitle } from "../constant/TabTitle"
import SideMenu from "./SideMenu"

const HeaderSection: React.FC = () => {
    const {
        contentState: { profileState }
    } = useGlobalContext()

    const {
        contentState: {
            activeTab
        }
    } = useHomeContext()

    const {
        contentState: {
            isOpenMenu, 
            setIsOpenMenu
        }
    } = useHomeContext()

    const handleClickMenu = () => {
        setIsOpenMenu(!isOpenMenu)
    }

    const getTitle = useMemo(() => {
        let title = ""
        if (activeTab == "pembayaran") {
            title = TabTitle.pembayaran
        }else if (activeTab == "pendanaan"){
            title = TabTitle.pendanaan
        }else if (activeTab == "transaksi"){
            title = TabTitle.transaksi
        }
        return title
    },[activeTab])

    return (
        <div className={`w-full bg-[#87F0FF] rounded-b-[60px] pb-2`}>
            <div className={`w-full ${activeTab=="simpan-pinjam" ? "min-h-[278px]" : "min-h-28" } bg-gradient-to-r from-[#4950F9] to-[#1937FE] rounded-b-[60px] px-8 pt-16 pb-8`}>
                <SideMenu/>
                <div className="flex flex-row items-center justify-between">
                    <div className="relative cursor-pointer" onClick={handleClickMenu}>
                        <Image src={HamburgerMenu} alt={"Menu"} className="w-[25px] h-[14px]"/>
                    </div>
                    {activeTab == "simpan-pinjam" ? (
                        <Image src={profileState?.image_url ? profileState?.image_url : photoProfile} alt={"Photo Profile"} width={48} height={48} className="relative object-cover w-12 h-12 rounded-lg"/>
                    ):(
                        <span className="w-full text-lg font-bold text-center text-white">{getTitle}</span>
                    )}
                </div>
                {activeTab == "simpan-pinjam" && (
                    <span className="ml-8 text-2xl font-normal text-white">Halo {profileState?.name},</span>
                )}
                <div className="flex flex-col items-center justify-center gap-4 mt-16">
                    <span className="text-xl font-normal text-[#87F0FF]">Total Transaksi Tahun Ini</span>
                    <span className="text-3xl font-bold text-white">Rp 0,00</span>
                </div>
            </div>
        </div>
    )
}

export default HeaderSection