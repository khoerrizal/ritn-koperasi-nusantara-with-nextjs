import BalanceCard from "./BalanceCard"
import BankAccountCard from "./BankAccountCard"
import HeaderSection from "./HeaderSection"

const SimpanPinjamScreen: React.FC = () => {
    return (
        <>
            <BalanceCard/>
            <BankAccountCard/>
        </>
    )
}

export default SimpanPinjamScreen