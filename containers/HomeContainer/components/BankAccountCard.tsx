import axios from "axios"
import Image from "next/image"
import { useEffect, useState } from "react"
import { useGlobalContext } from "../../../providers/globalProvider"
import { useHomeContext } from "../../../providers/homeProvider"
import { BankCard, smallArrowRight } from "../../../public/images/HomePage"

const BankAccountCard: React.FC = () => {
    const {
        contentState: {
            setActiveTab
        }
    } = useHomeContext()
    const {
        authState: {
            user
        },
        contentState: {
            transaksiList
        }
    } = useGlobalContext()
    const [buttonLabel, setButtonLabel] = useState<"Ajukan Pendanaan" | "Pengajuan dalam proses">("Ajukan Pendanaan")
    
    useEffect(() => {
        const draft = transaksiList.filter((line) => {return !line.status})
        if (draft.length > 0){
            setButtonLabel("Pengajuan dalam proses")
        }
    }, [transaksiList]);
    return (
        <div onClick={() => setActiveTab(buttonLabel == "Ajukan Pendanaan" ? "pendanaan" : "transaksi")} className="mt-[350px] relative justify-center items-center flex flex-row gap-20 shadow-2xl cursor-pointer">
            <Image src={BankCard} alt={"Bank Card"} className="z-0 w-full h-[146px] absolute"/>
            <span className="z-50 text-xl font-normal text-center text-white">{buttonLabel}</span>
            <div className="relative">
                <Image src={smallArrowRight} alt={"Small Arrow Right"} className="w-[7px] h-3"/>
            </div>
        </div>
    )
}

export default BankAccountCard