import { Listbox, Transition } from "@headlessui/react"
import axios from "axios"
import router from "next/router"
import { Fragment, useEffect, useMemo, useState } from "react"
import { useGlobalContext } from "../../../providers/globalProvider"
import { useHomeContext } from "../../../providers/homeProvider"

const FormPendanaan: React.FC = () => {
    const {
        authState: { user },
        contentState: {
            getTransaksiList
        }
    } = useGlobalContext()
    const {
        contentState: {
            setActiveTab
        }
    } = useHomeContext()
    const [cicilan, setCicilan] = useState<number>(0)
    const [jumlah, setJumlah] = useState<number>(0)
    const [tujuan, setTujuan] = useState<string>("")
    const [errorMsg, setErrorMsg] = useState<string>("")
    const [disableButton, setDisableButton] = useState<boolean>(true)
    const tenorOptions = [
        {
            value: 1,
            label: "1 bulan"
        },
        {
            value: 2,
            label: "2 bulan"
        },
        {
            value: 3,
            label: "3 bulan"
        },
    ]
    const [tenor, setTenor] = useState<{value: number, label: string}>(tenorOptions[0])

    useEffect(() => {
        const beban = Math.round((jumlah + (jumlah * 10 /100)) / (tenor?.value ? tenor?.value : 1))
        setCicilan(beban)
    }, [jumlah, tenor])

    const handleOnchangeJumlah = (event:any) => {
        const result = parseInt(event.target.value.replace(/\D/g, ''))
        setJumlah(result ? result : 0)
    }

    const handleOnchangeTujuan = (event: any) => {
        setTujuan(event.target.value)
    }

    const submitPendanaan = async (event:any) => {
        event.preventDefault()
        try {
            const data = {
                user_id: user.uid,
                amount: jumlah,
                tenor: tenor,
                tujuan: tujuan,
                status: "new"
            }
            const { data : res} = await axios.post('/api/pendanaan', data);
            getTransaksiList()
            console.log(res, "response submit pendanaan")
            if (res.id){
                setActiveTab("simpan-pinjam")
            }else{
                setErrorMsg("Error on send request")
            }
        } catch (error: any) {
            console.log(error.message, "error on signup");
            if (error.message.indexOf("email-already-in-use") != -1){
                setErrorMsg("Email already in use")
            }
        }
    };

    useEffect(() => {
        if (!jumlah || !tenor || !tujuan){
            setDisableButton(true)
        }else{
            setDisableButton(false)
        }
    }, [jumlah, tenor, tujuan]);

    return (
        <form onSubmit={submitPendanaan} className="relative flex flex-col justify-between gap-2 mt-4 mb-28 mx-9">
            <div className="flex flex-col gap-5">
                <div className="flex flex-col gap-4">
                    <label className="text-lg font-normal text-[#2743FD]">Tujuan Pendanaan</label>
                    <input required value={tujuan} onChange={handleOnchangeTujuan} className="border-b border-b-[#DEE1DF] text-[#B6BFFF] font-bold text-lg p-1"/>
                </div>
                <div className="flex flex-col gap-4">
                    <label className="text-lg font-normal text-[#2743FD]">Jumlah Pendanaan</label>
                    <input required value={jumlah.toLocaleString()} onChange={handleOnchangeJumlah} className="border-b border-b-[#DEE1DF] text-[#B6BFFF] font-bold text-4xl p-1"/>
                </div>
                <div className="flex flex-col gap-4">
                    <label className="text-lg font-normal text-[#2743FD]">Tenor</label>
                    <Listbox value={tenor} onChange={setTenor}>
                        <div className="relative mt-1">
                        <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                            <span className="block truncate">{tenor?.label ?? "Pilih tenor"}</span>
                            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                            </span>
                        </Listbox.Button>
                        <Transition
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                            {tenorOptions.map((item, itemIdx) => (
                                <Listbox.Option
                                key={itemIdx}
                                className={({ active }) =>
                                    `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                    active ? 'bg-amber-100 text-amber-900' : 'text-gray-900'
                                    }`
                                }
                                value={item}
                                >
                                {({ selected }) => (
                                    <>
                                    <span
                                        className={`block truncate ${
                                        selected ? 'font-medium' : 'font-normal'
                                        }`}
                                    >
                                        {item.label}
                                    </span>
                                    {selected ? (
                                        <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                        </span>
                                    ) : null}
                                    </>
                                )}
                                </Listbox.Option>
                            ))}
                            </Listbox.Options>
                        </Transition>
                        </div>
                    </Listbox>
                </div>
                <div className="flex flex-col gap-4">
                    <label className="text-lg font-normal text-[#2743FD]">Cicilan</label>
                    <span className="border-b border-b-[#DEE1DF] text-[#3A3A3A] text-base font-normal">
                        {cicilan.toLocaleString()}
                    </span>
                </div>
            </div>
            {errorMsg && (
                <span className="bg-red-600">{errorMsg}</span>
            )}
            <button disabled={disableButton} type="submit" className="w-full py-6 border border-[#2743FD] rounded-3xl bg-blue-600 disabled:bg-gray-800 text-white">
                Kirim Pengajuan
            </button>
        </form>
    )
}

export default FormPendanaan