import Image from "next/image";
import { useGlobalContext } from "../../../providers/globalProvider";
import { searchIcon } from "../../../public/images/HomePage";

const TransaksiScreen: React.FC = () => {
    const {
        contentState: {
            transaksiList
        }
    } = useGlobalContext()
    return (
        <div className="w-full h-full bg-gradient-to-b from-[#1937FE] to-[#4950F9] mt-5 rounded-t-[60px] pt-4 items-center flex flex-col gap-6 px-7">
            <div className="w-12 h-1 bg-white rounded"/>
            <div className="relative">
                <input className="bg-[#05199E] text-[#3D56FA] w-full py-4 pl-14 pr-4 rounded-2xl" placeholder="Cari"/>
                <div className="absolute w-5 h-5 top-4 left-4">
                    <Image src={searchIcon} alt={"Search Icon"} className="absolute" fill/>
                </div>
            </div>
            <div className="flex flex-col w-full h-64 gap-2 pr-1 overflow-y-scroll">
                {transaksiList.map((line, index) => (
                    <div key={index} className="flex flex-row items-center justify-between w-full text-lg font-normal text-white">
                        <div className="flex flex-col">
                            <span>{line.tujuan}</span>
                            <span className="text-[#80E0FF]">{line.created}</span>
                        </div>
                        <span>Rp {line.amount.toLocaleString()}</span>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default TransaksiScreen