import Image from "next/image"
import { ColumnsGraph } from "../../../public/images/HomePage"

const BalanceCard: React.FC = () => {
    return (
        <div className="w-full h-[338px] absolute top-0 px-6 mt-48">
            <div className="w-full h-full bg-white shadow-2xl rounded-[40px] flex flex-col px-9 py-8">
                <span className="text-[#3A3A3A] text-base font-normal">Saldo Anda</span>
                <span className="text-[#2D99FF] font-bold text-3xl mt-2">Rp 0,00</span>
                <div className="relative mt-7">
                    <Image src={ColumnsGraph} alt={"Column Graph"} className="w-[255px] h-[183px]"/>
                </div>
            </div>
        </div>
    )
}

export default BalanceCard