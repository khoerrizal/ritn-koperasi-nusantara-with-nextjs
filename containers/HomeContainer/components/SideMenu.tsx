import { Dialog } from '@headlessui/react'
import Image from "next/image"
import { useRouter } from 'next/router'
import { useCookies } from 'react-cookie'
import SmallArrowRight from '../../../components/atoms/SmallArrowRight'
import { useGlobalContext } from '../../../providers/globalProvider'
import { useHomeContext } from '../../../providers/homeProvider'
import { logOutIcon, paymentIcon, simpanPinjamIcon, transactionIcon } from '../../../public/images/HomePage'
import { photoProfile } from '../../../public/images/ProfilePage'

const SideMenu: React.FC = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['user']);
    const router = useRouter();
    const {
        contentState: {
            profileState
        },
        authState: {
            user, logOut
        }
    } = useGlobalContext()
    const {
        contentState: {
            isOpenMenu, 
            setIsOpenMenu,
            activeTab, 
            setActiveTab
        }
    } = useHomeContext()

    const handleLogout = async () => {
        try {
            await logOut();
            removeCookie('user')
            router.push("/login");
        } catch (error: any) {
            console.log(error.message);
        }
    };

    const handleClickSimpanPinjam = () => {
        setActiveTab("simpan-pinjam")
        setIsOpenMenu(false)
    }

    const handleClickTransaksi = () => {
        setActiveTab("transaksi")
        setIsOpenMenu(false)
    }

    const handleClickPembayaran = () => {
        setActiveTab("pembayaran")
        setIsOpenMenu(false)
    }

    return (
        <Dialog open={isOpenMenu} onClose={() => setIsOpenMenu(false)} className="absolute top-0 z-50 flex items-center justify-center w-full h-full">
            <Dialog.Panel className={"bg-white w-[375px] h-full rounded-lg relative flex flex-col items-center mr-44 justify-between"}>
                <div className='flex flex-col w-full mt-16'>
                    <div className='flex flex-row w-full gap-2 px-6'>
                        <Image src={profileState?.image_url ? profileState?.image_url : photoProfile} alt={"Photo Profile"} width={48} height={48} className="relative object-cover w-12 h-12 rounded-lg"/>
                        <div className='flex flex-col text-[#3A3A3A] truncate w-40'>
                            <span className='text-base font-bold'>{profileState?.name}</span>
                            <span className='text-sm font-normal'>{user.email}</span>
                        </div>
                    </div>
                    <div className='flex flex-col text-[#2B47FC] mt-9 font-normal text-lg'>
                        <div onClick={handleClickSimpanPinjam} className={`${ activeTab == "simpan-pinjam" ? "bg-[#F2F4F8]": ""} flex flex-row items-center justify-between py-5 px-6 cursor-pointer`}>
                            <div className='flex flex-row gap-2'>
                                <Image src={simpanPinjamIcon} alt={'Simpan Pinjam Icon'} className="relative" width={24} height={24}/>
                                <span>Simpan Pinjam</span>
                            </div>
                            <SmallArrowRight fillColor='#2B47FC'/>
                        </div>
                        <div onClick={handleClickTransaksi} className={`${ activeTab == "transaksi" ? "bg-[#F2F4F8]": ""} flex flex-row items-center justify-between py-5 px-6 cursor-pointer`}>
                            <div className='flex flex-row gap-2'>
                                <Image src={transactionIcon} alt={'Transaksi Icon'} className="relative" width={24} height={24}/>
                                <span>Transaksi</span>
                            </div>
                            <SmallArrowRight fillColor='#2B47FC'/>
                        </div>
                        <div onClick={handleClickPembayaran} className={`${ activeTab == "pembayaran" ? "bg-[#F2F4F8]": ""} flex flex-row items-center justify-between py-5 px-6 cursor-pointer`}>
                            <div className='flex flex-row gap-2'>
                                <Image src={paymentIcon} alt={'Pembayaran Icon'} className="relative" width={24} height={24}/>
                                <span>Pembayaran</span>
                            </div>
                            <SmallArrowRight fillColor='#2B47FC'/>
                        </div>
                    </div>
                </div>
                <div className='w-full px-8 mb-14'>
                    <button onClick={handleLogout} className='w-full border border-[#2743FD] p-7 rounded-3xl flex flex-row justify-between items-center'>
                        <span className='text-xl font-normal text-[#2743FD]'>Keluar</span>
                        <Image src={logOutIcon} alt={'Log out icon'} width={21} height={22}/>
                    </button>
                </div>
            </Dialog.Panel>
        </Dialog>
    )
}

export default SideMenu