import axios from 'axios'
import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signOut } from 'firebase/auth'
import { useRouter } from 'next/router'
import { createContext, useContext, useEffect, useState } from 'react'
import { useCookies } from 'react-cookie'
import { auth } from '../../pages/config/firebase'
import {
    IAuthState,
    IContentState,
    IGlobalState, INotificationState, IProfileState, IProviderProps, ITransaksiState, UserType
} from './types'

export function createCtx<IGlobalState>() {
    const ctx = createContext<IGlobalState | undefined>(undefined)
    function useCtx() {
        const c = useContext(ctx)
        if (!c) throw new Error('useCtx must be inside a Provider with a value')
        return c
    }
    return [useCtx, ctx.Provider] as const
}

export const [useGlobalContext, CtxProvider] = createCtx<IGlobalState>()

export const GlobalProvider: React.FC<IProviderProps> = ({ children }) => {
    const [successMsg, setSuccessMsg] = useState<string>('tes success')
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [user, setUser] = useState<UserType>({ email: null, uid: null });
    const [loading, setLoading] = useState<boolean>(true);
    const [cookies, setCookie, removeCookie] = useCookies(['user']);
    const [profileState, setProfileState] = useState<IProfileState | null>(null)
    const router = useRouter()
    const [transaksiList, setTransaksiList] = useState<ITransaksiState[]>([])

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                setUser({
                email: user.email,
                uid: user.uid,
                });
            } else {
                setUser({ email: null, uid: null });
            }
        });
        setLoading(false);
        if (cookies && cookies.user){
            setUser(cookies.user)
            getProfile(cookies.user.uid)
        }else{
            router.push('/login')
        }

        return () => unsubscribe();
    }, []);

    const getTransaksiList = async () => {
        if (user.uid){
            const { data : res} = await axios.get(`/api/pendanaan/${user.uid}`);
            setTransaksiList(res)
        }
    }

    useEffect(() => {
        if(user){
            getTransaksiList()
        }
    }, [user])

    const getProfile = async (user_id: string) => {
        const { data : res} = await axios.get(`/api/users/${user_id}`)
        setProfileState(res)
    }

    const signUp = (email: string, password: string) => {
        return createUserWithEmailAndPassword(auth, email, password);
    };

    const logIn = async (email: string, password: string) => {
        const res = await signInWithEmailAndPassword(auth, email, password);
        setUser(res.user)
        getProfile(res.user.uid)
        return res
    };

    const logOut = async () => {
        setUser({ email: null, uid: null });
        await signOut(auth);
    };
    
    const notificationState: INotificationState = {
        successMsg,
        errorMsg,
        setErrorMsg,
    }

    const authState: IAuthState = {
        user: user,
        signUp: signUp,
        logIn: logIn,
        logOut: logOut,
    }

    const contentState: IContentState = {
        profileState,
        getProfile,
        transaksiList,
        getTransaksiList
    }

    const state: IGlobalState = {
        notificationState,
        authState,
        contentState
    }

    return <CtxProvider value={state}>{children}</CtxProvider>
}