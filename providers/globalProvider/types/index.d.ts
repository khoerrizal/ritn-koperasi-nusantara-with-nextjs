export interface IProviderProps {
    children: JSX.Element
}

export interface UserType {
    email: string | null;
    uid: string | null;
}

export interface INotificationState {
    successMsg: string
    errorMsg: string
    setErrorMsg: (msg: string) => void
}

export interface ITransaksiState {
    user_id: string,
    amount: number,
    tenor: {
        value: number,
        label: string
    },
    created: string
    tujuan: string
    status?: "approved" | "rejected"
}

export interface IContentState {
    profileState: IProfileState | null
    getProfile: async
    transaksiList: ITransaksiState[]
    getTransaksiList: async
}

export interface IProfileState {
    id: string
    user_id: string
    name: string
    date_of_birth: string
    image_url: string
    created?: Date
    updated?: Date
}

export interface IAuthState {
    user: UserType
    signUp: (email: string, password: string) => any
    logIn: (email: string, password: string) => any
    logOut: async
}

export interface IGlobalState {
    notificationState: INotificationState
    authState: IAuthState
    contentState: IContentState
}