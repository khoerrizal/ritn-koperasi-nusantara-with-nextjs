export interface INotificationState {
    successMsg: string
    errorMsg: string
}

export interface IContentState {
    isOpenMenu: boolean
    setIsOpenMenu: (state: boolean) => void
    activeTab: "simpan-pinjam" | "transaksi" | "pembayaran" | "pendanaan"
    setActiveTab: (state: "simpan-pinjam" | "transaksi" | "pembayaran" | "pendanaan") => void
}

export interface IHomeState {
    notificationState: INotificationState,
    contentState: IContentState
}
