import HamburgerMenu from './Hamburger Menu.svg'
import ColumnsGraph from './ColumnsGraph.svg'
import BankCard from './Bank account card.png'
import smallArrowRight from './Small Arrow Right.svg'
import paymentIcon from './PaymentIcon.svg'
import simpanPinjamIcon from './SimpanPinjamIcon.svg'
import transactionIcon from './TransactionIcon.svg'
import logOutIcon from './LogOutIcon.svg'
import searchIcon from './SearchIcon.svg'

export {
    HamburgerMenu,
    ColumnsGraph,
    BankCard,
    smallArrowRight,
    paymentIcon,
    simpanPinjamIcon,
    transactionIcon,
    logOutIcon,
    searchIcon
}