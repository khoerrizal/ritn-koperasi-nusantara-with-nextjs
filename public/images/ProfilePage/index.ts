import backgroundProfile from './Background Profile.svg'
import photoProfile from './Profile image.svg'
import checkIcon from './Check Icon.svg'

export {
    backgroundProfile,
    photoProfile,
    checkIcon
}