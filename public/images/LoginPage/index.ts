import buttonMask from './Button mask.png'
import backgroundLogin from './BackgroundLogin.png'

export {
    buttonMask,
    backgroundLogin
}